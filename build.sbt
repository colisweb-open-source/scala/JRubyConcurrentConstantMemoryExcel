import CompileFlags.crossScalacOptions

lazy val scala212               = "2.12.11"
lazy val scala213               = "2.13.5"
lazy val supportedScalaVersions = List(scala213, scala212)

Global / onChangedBuildSource := ReloadOnSourceChanges

ThisBuild / organization := "com.colisweb"
ThisBuild / scalaVersion := scala213
ThisBuild / scalafmtOnCompile := true
ThisBuild / scalafmtCheck := true
ThisBuild / scalafmtSbtCheck := true
ThisBuild / pushRemoteCacheTo := Some(
  MavenCache("local-cache", baseDirectory.value / sys.env.getOrElse("CACHE_PATH", "sbt-cache"))
)
scalacOptions ~= (_.filterNot(Set("-Ywarn-inaccessible")))
crossScalaVersions := supportedScalaVersions

lazy val projectName = "JRubyConcurrentConstantMemoryExcel"

lazy val testKitLibs = Seq(
  "org.scalacheck" %% "scalacheck" % "1.14.0",
  "org.scalactic"  %% "scalactic"  % "3.2.9",
  "org.scalatest"  %% "scalatest"  % "3.2.9",
).map(_ % Test)

lazy val poi =
  ((version: String) =>
    Seq(
      "org.apache.poi" % "poi"       % version,
      "org.apache.poi" % "poi-ooxml" % version
    ))("4.1.0")

lazy val monix =
  ((version: String) =>
    Seq(
      "io.monix" %% "monix-execution" % version,
      "io.monix" %% "monix-eval"      % version,
    ))("3.3.0")

lazy val root =
  Project(id = projectName, base = file("."))
    .settings(moduleName := "root")
    .settings(crossScalaVersions := supportedScalaVersions)
    .settings(noPublishSettings: _*)
    .aggregate(core)
    .dependsOn(core)

lazy val core =
  project
    .settings(moduleName := projectName)
    .settings(crossScalaVersions := supportedScalaVersions)
    .settings(
      libraryDependencies ++= Seq(
        "com.nrinaudo"         %% "kantan.csv"   % "0.6.0",
        "com.github.pathikrit" %% "better-files" % "3.8.0",
      ) ++ monix ++ poi ++ testKitLibs)

/**
  * Copied from Cats
  */
lazy val noPublishSettings = Seq(
  publish := {},
  publishLocal := {},
  publishArtifact := false
)

//// Aliases

/**
  * Copied from kantan.csv
  */
addCommandAlias("runBenchs", "benchmarks/jmh:run -i 10 -wi 10 -f 2 -t 1")

/**
  * Example of JMH tasks that generate flamegraphs.
  *
  * http://malaw.ski/2017/12/10/automatic-flamegraph-generation-from-jmh-benchmarks-using-sbt-jmh-extras-plain-java-too/
  */
addCommandAlias(
  "flame93",
  "benchmarks/jmh:run -f1 -wi 10 -i 20 PackerBenchmarkWithRealData -prof jmh.extras.Async:flameGraphOpts=--minwidth,2;verbose=true")
